# WoSManifestLib

The **Wings of Steel Manifest Library** is an auxiliary library which provides shared functionality for handling Manifest XML files. It is used by [WoSLauncher](https://gitlab.com/hostile.node/WoSLauncher) and [WoSPacker](https://gitlab.com/hostile.node/WoSPacker).

A Manifest contains a list of files: their name, size, hash and optionally the size of the file after it is compressed. This is used to compare a list of local files with a list of files in a remote server and figure out which files have to be downloaded.

# Credits

Project icon by [Freepik](https://www.freepik.com) from [FlatIcon](https://www.flaticon.com/), licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).
