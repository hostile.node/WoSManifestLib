﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using System.Xml;

namespace WoSManifestLib
{
    public class ManifestEntry : IEquatable<ManifestEntry>
    {
        public ManifestEntry()
        {
			m_Size = 0;
            m_CompressedSize = 0;
        }

		public void GenerateFromFile(String rootDirectory, String filename)
		{
			m_ShortFilename = filename.Substring(rootDirectory.Length + 1); // Strip the root directory
			m_FullFilename = filename;
			m_Hash = GenerateHash(filename);
            m_Size = (new FileInfo(filename)).Length;
		}

        public void GenerateFromXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.IsStartElement() && !reader.IsEmptyElement)
                {
					if (reader.Name == "Name")
					{
						reader.Read();
						m_ShortFilename = reader.Value;
                        m_FullFilename = reader.Value;
					}
                    else if (reader.Name == "Size")
                    {
                        reader.Read();
                        m_Size = Convert.ToUInt32(reader.Value);
                    }
                    else if (reader.Name == "CompressedSize")
                    {
                        reader.Read();
                        m_CompressedSize = Convert.ToUInt32(reader.Value);
                    }
                    else if (reader.Name == "Hash")
                    {
                        reader.Read();
                        m_Hash = reader.Value;
                    }
                }
                else if (reader.Name == "File" && !reader.IsStartElement())
                {
                    break;
                }
            }
        }

		public String ShortFilename
        {
			get { return m_ShortFilename; }
			set { m_ShortFilename = value; }
		}

        public String FullFilename
        {
			get { return m_FullFilename; }
			set { m_FullFilename = value; }
		}

		public String Hash
		{
			get { return m_Hash; }
			set { m_Hash = value; }
		}

		public long Size
		{
			get { return m_Size; }
			set { m_Size = value; }
		}

        public long CompressedSize
        {
            get { return m_CompressedSize; }
            set { m_CompressedSize = value; }
        }

        public bool Equals(ManifestEntry other)
        {
            return (this.ShortFilename == other.ShortFilename &&
                    this.Size == other.Size &&
                    this.Hash == other.Hash);
        }

		private String GenerateHash(String filename)
        {
			SHA1 sha1Hash = SHA1.Create();
            System.IO.StreamReader streamReader = new System.IO.StreamReader(filename);

            // Convert the input string to a byte array and compute the hash.
            byte[] data = sha1Hash.ComputeHash(streamReader.BaseStream);

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder stringBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append(data[i].ToString("x2"));
            }

            streamReader.Close();

            // Return the hexadecimal string.
            return stringBuilder.ToString();
        }

		private String m_FullFilename;
        private String m_ShortFilename;
        private String m_Hash;
        private long m_Size;
        private long m_CompressedSize;
    }
}
