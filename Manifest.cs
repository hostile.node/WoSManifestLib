﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace WoSManifestLib
{
    public class Manifest
    {
        // Creates a manifest from a given directory. 
        // All files and sub-directories are recursively added.
        // If the manifest already contained data, this data is discarded.
		public bool GenerateFromDirectory(String rootDirectory)
		{
			m_RootDirectory = rootDirectory;
			m_Files = new List<ManifestEntry>();
			return GenerateManifestEntries(rootDirectory);
		}

        // Creates a manifest from a previously existing manifest XML file.
		public void GenerateFromManifestFile(String filename)
		{
            m_RootDirectory = ".";
            m_Files = new List<ManifestEntry>();

            if (File.Exists(filename))
            {
                XmlReader reader = XmlReader.Create(filename);
                while (reader.Read())
                {
                    if (reader.IsStartElement() && !reader.IsEmptyElement)
                    {
						if (reader.Name == "File")
						{
							ManifestEntry entry = new ManifestEntry();
                            entry.GenerateFromXml(reader);
                            m_Files.Add(entry);
						}
                    }
                }
            }
		}

        public void GenerateFromExclusion(Manifest left, Manifest right)
        {
            m_RootDirectory = ".";
            m_Files = new List<ManifestEntry>();

            foreach (ManifestEntry entry in left.Entries)
            {
                if (right.Entries.Contains(entry) == false)
                {
                    m_Files.Add(entry);
                }
            }
        }

        // Writes this manifest to a XML file.
        public void WriteToFile(String filename)
        {
            // These writer settings allow XmlWriter to generate a file that is properly
            // idented, rather than writing every element into a single line.
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };

            XmlWriter writer = XmlWriter.Create(filename, settings);
            writer.WriteStartDocument();
            writer.WriteStartElement("Files");
            foreach (ManifestEntry entry in m_Files)
            {
                writer.WriteStartElement("File");
                writer.WriteElementString("Name", entry.ShortFilename);
                writer.WriteElementString("Size", entry.Size.ToString());
                writer.WriteElementString("CompressedSize", entry.CompressedSize.ToString());
                writer.WriteElementString("Hash", entry.Hash);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        public List<ManifestEntry> Entries
        {
            get { return m_Files; }
        }

        private bool GenerateManifestEntries(String rootDirectory)
		{
			try
			{
				// Recursively gather more directories and files
				String[] directories = System.IO.Directory.GetDirectories(rootDirectory);
				foreach (String directory in directories)
				{
					GenerateManifestEntries(directory);
				}

				String[] dirFiles = System.IO.Directory.GetFiles(rootDirectory);
				foreach (String filename in dirFiles)
				{
					ManifestEntry entry = new ManifestEntry();
					entry.GenerateFromFile(m_RootDirectory, filename);
					m_Files.Add(entry);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private List<ManifestEntry> m_Files;
		private String m_RootDirectory;
    }
}
